# Educational Project on some bean features in Spring

## 1. Annotation instead of ```@Qualifier``` for adding specific Beans of some type in collection

- Creating a custom annotation
- Creating a BeanPostProcessor implementation
- Creating a config class to inject BPP
- Creating an autoconfig at resources/META-INF/spring.factories

## 2. Registering some legacy Beans from jar with some legacy annotation defining Beans

- Have a jar with some Bean classes with some legacy annotation saying they're Beans
- Creating a ImportBeanDefinitionRegistrar implementation for adding parsing that jar, 
searching for such Bean classes, registering them as Bean definitions
- Using ```@Import``` with our Registrar implementation on ```@Configuration``` class for adding it on the early stage when
definitions are created

## 3. Injecting new Bean during the runtime by its name, class name and class file

- Creating a BeanRequest with beanName, beanClassName and Scope for the future Bean
- Making sure the future Bean .class file is on the path which was stated in code
- Creating a RestController for adding the future bean by loading the class, registering bean definition, 
using getBean method for loading in context
- Making some preparations for such injections in order to get this new bean injected in collections of the Beans
of this type where it is a dependency
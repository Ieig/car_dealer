package com.lebedev.spring_bean_dealer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBeanDealerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBeanDealerApplication.class, args);
    }
}

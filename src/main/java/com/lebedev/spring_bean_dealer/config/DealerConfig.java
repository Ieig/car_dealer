package com.lebedev.spring_bean_dealer.config;

import com.lebedev.spring_bean_dealer.bdr.ChineseBeanDefinitionRegistrar;
import com.lebedev.spring_bean_dealer.bpp.InjectListBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ChineseBeanDefinitionRegistrar.class)
public class DealerConfig {

    @Bean
    public InjectListBeanPostProcessor injectListBeanPostProcessor() {
        return new InjectListBeanPostProcessor();
    }

}

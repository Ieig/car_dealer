package com.lebedev.spring_bean_dealer.service.city;

import com.lebedev.spring_bean_dealer.model.Client;
import com.lebedev.spring_bean_dealer.service.dealer.ChineseDealer;
import com.lebedev.spring_bean_dealer.service.dealer.Dealer;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class Prague implements City {
    Map<String, Dealer> dealers = new HashMap<>();

    @Override
    public void register(String type, Dealer dealer) {
        dealers.put(type, dealer);
    }

    @Override
    public void chooseDealer(Client client) {
        Dealer dealer = dealers.getOrDefault(client.getWish(), new ChineseDealer());
        dealer.showCar();
    }
}

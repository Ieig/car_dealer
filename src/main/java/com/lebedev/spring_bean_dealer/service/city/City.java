package com.lebedev.spring_bean_dealer.service.city;

import com.lebedev.spring_bean_dealer.model.Client;
import com.lebedev.spring_bean_dealer.service.dealer.Dealer;

public interface City {
    void register(String type, Dealer dealer);
    void chooseDealer(Client client);
}

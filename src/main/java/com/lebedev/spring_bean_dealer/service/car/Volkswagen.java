package com.lebedev.spring_bean_dealer.service.car;

import com.lebedev.legacy.chinese.Car;
import org.springframework.stereotype.Component;

@Component
public class Volkswagen implements Car {
    @Override
    public void carSays() {
        System.out.println("Das Auto");
    }
}

package com.lebedev.spring_bean_dealer.service.car;

import com.lebedev.legacy.chinese.Car;
import org.springframework.stereotype.Component;

@Component
public class Audi implements Car {
    @Override
    public void carSays() {
        System.out.println("Vorsprung durch Technik");
    }
}

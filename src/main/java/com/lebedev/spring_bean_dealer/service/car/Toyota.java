package com.lebedev.spring_bean_dealer.service.car;

import com.lebedev.spring_bean_dealer.annotation.Toyotaz;
import com.lebedev.legacy.chinese.Car;
import org.springframework.stereotype.Component;

@Component
@Toyotaz
public class Toyota implements Car {
    @Override
    public void carSays() {
        System.out.println("Always A Better Way");
    }
}

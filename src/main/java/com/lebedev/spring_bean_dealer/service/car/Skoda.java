package com.lebedev.spring_bean_dealer.service.car;

import com.lebedev.legacy.chinese.Car;
import org.springframework.stereotype.Component;

@Component
public class Skoda implements Car {
    @Override
    public void carSays() {
        System.out.println("Simply Clever");
    }
}

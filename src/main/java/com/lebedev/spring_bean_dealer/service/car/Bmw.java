package com.lebedev.spring_bean_dealer.service.car;

import com.lebedev.legacy.chinese.Car;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("bmw")
public class Bmw implements Car {
    @Override
    public void carSays() {
        System.out.println("Freude am Fahren");
    }
}

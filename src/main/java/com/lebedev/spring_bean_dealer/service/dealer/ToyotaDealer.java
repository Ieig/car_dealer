package com.lebedev.spring_bean_dealer.service.dealer;

import com.lebedev.spring_bean_dealer.annotation.Toyotaz;
import com.lebedev.legacy.chinese.Car;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

// Bean for registration at runtime
public class ToyotaDealer implements Dealer {
    @Autowired
    @Toyotaz
    private List<Car> cars;

    @Override
    public String getName() {
        return "reliable";
    }

    @Override
    public void showCar() {
        System.out.println("You want " + getName() + "? Look what we have:");
        cars.forEach(Car::carSays);
    }
}

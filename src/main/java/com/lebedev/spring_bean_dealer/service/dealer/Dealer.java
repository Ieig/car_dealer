package com.lebedev.spring_bean_dealer.service.dealer;

import com.lebedev.spring_bean_dealer.service.city.City;
import org.springframework.beans.factory.annotation.Autowired;

public interface Dealer {
    String getName();
    void showCar();

    @Autowired
    default void regMe(City city) {
        city.register(getName(), this);
    }
}

package com.lebedev.spring_bean_dealer.service.dealer;

import com.lebedev.legacy.chinese.Car;
import com.lebedev.spring_bean_dealer.annotation.ChineseLegacy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChineseDealer implements Dealer {
    @Autowired
    @ChineseLegacy
    List<Car> cars;

    @Override
    public String getName() {
        return "chip";
    }

    @Override
    public void showCar() {
        System.out.println("You want " + getName() + "? Look what we have:");
        cars.forEach(Car::carSays);
    }
}

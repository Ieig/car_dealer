package com.lebedev.spring_bean_dealer.service.dealer;

import com.lebedev.legacy.chinese.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BmwDealer implements Dealer {
    @Autowired
    @Qualifier("bmw")
    private List<Car> cars;

    @Override
    public String getName() {
        return "fast";
    }

    @Override
    public void showCar() {
        System.out.println("You want " + getName() + "? Look what we have:");
        cars.forEach(Car::carSays);    }
}

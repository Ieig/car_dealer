package com.lebedev.spring_bean_dealer.service.dealer;

import com.lebedev.spring_bean_dealer.service.car.Audi;
import com.lebedev.spring_bean_dealer.service.car.Skoda;
import com.lebedev.spring_bean_dealer.service.car.Volkswagen;
import com.lebedev.legacy.chinese.Car;
import com.lebedev.spring_bean_dealer.annotation.InjectList;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VagDealer implements Dealer {
    @InjectList(classes = {Audi.class, Volkswagen.class, Skoda.class})
    private List<Car> cars;

    @Override
    public String getName() {
        return "vag";
    }
    @Override
    public void showCar() {
        System.out.println("You want " + getName() + "? Look what we have:");
        cars.forEach(Car::carSays);
    }
}

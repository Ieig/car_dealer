package com.lebedev.spring_bean_dealer.bpp;

import com.lebedev.spring_bean_dealer.annotation.InjectList;
import lombok.SneakyThrows;
import org.reflections.ReflectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;

import java.beans.Introspector;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class InjectListBeanPostProcessor implements BeanPostProcessor {
    @Autowired
    private ApplicationContext context;

    @SneakyThrows
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Set<Field> fields = ReflectionUtils.getAllFields(bean.getClass(), field -> field.isAnnotationPresent(InjectList.class));
        for (Field field : fields) {
            InjectList annotation = field.getAnnotation(InjectList.class);
            List<Object> beanList = Arrays.stream(annotation.classes())
                    .map(aClass -> Introspector.decapitalize(aClass.getSimpleName()))
                    .map(name -> context.getBean(name))
                    .toList();
            field.setAccessible(true);
            field.set(bean, beanList);
        }
        return bean;
    }
}

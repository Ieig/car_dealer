package com.lebedev.spring_bean_dealer.api.controller;

import com.lebedev.spring_bean_dealer.api.request.BeanRequest;
import com.lebedev.spring_bean_dealer.auxilary.CustomClassLoader;
import com.lebedev.spring_bean_dealer.model.Client;
import com.lebedev.spring_bean_dealer.service.city.City;
import com.lebedev.spring_bean_dealer.service.dealer.Dealer;
import com.lebedev.spring_bean_dealer.service.dealer.ToyotaDealer;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/beans")
public class BeanRegisterController {

    @Autowired
    private GenericApplicationContext context;

    @Autowired
    private CustomClassLoader customClassLoader;

    @SneakyThrows
    @PostMapping("/reg")
    public String registerBean(@RequestBody BeanRequest request) {
        Class<?> beanClass = customClassLoader.loadClass(request.getBeanClassName());
        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) context.getBeanFactory();

        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setScope(request.getBeanScope());
        beanDefinition.setBeanClass(beanClass);
        beanFactory.registerBeanDefinition(request.getBeanName(), beanDefinition);

        context.getBean(request.getBeanName());

        return "registered";
    }

    @GetMapping("/show")
    public List<String> showDealers() {
        return Arrays.stream(context.getBeanNamesForType(Dealer.class)).toList();
    }

    @GetMapping("/test")
    public void showDealerCars() {
        System.out.println("\n\n Now testing the city has toyota inside:");
        City city = context.getBean(City.class);
        city.chooseDealer(new Client("reliable"));

        System.out.println("\n\n Test bean has a dependent car:");
        ToyotaDealer bean = (ToyotaDealer) context.getBean("toyotaDealer");
        bean.showCar();
    }
}

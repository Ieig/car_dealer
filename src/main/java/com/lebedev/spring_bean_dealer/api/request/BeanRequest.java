package com.lebedev.spring_bean_dealer.api.request;

import lombok.Data;

@Data
public class BeanRequest {
    private String beanName;
    private String beanClassName;
    private String beanScope;
}

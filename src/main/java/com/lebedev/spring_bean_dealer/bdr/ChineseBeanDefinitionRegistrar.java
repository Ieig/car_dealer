package com.lebedev.spring_bean_dealer.bdr;

import com.lebedev.legacy.chinese.ChineseSingleton;
import com.lebedev.spring_bean_dealer.annotation.ChineseLegacy;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AutowireCandidateQualifier;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.beans.Introspector;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ChineseBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        Set<Class<?>> classes = new HashSet<>();

        try (ScanResult scanResult = new ClassGraph()
                .enableAllInfo()
                .acceptPackages("com.lebedev.legacy.chinese")
                .scan()) {

            classes = scanResult.getClassesWithAnnotation(ChineseSingleton.class.getName()).stream()
                    .map(ClassInfo::loadClass)
                    .collect(Collectors.toSet());

        }

        for (Class<?> aClass : classes) {

            GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
            beanDefinition.setBeanClass(aClass);
            beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
            beanDefinition.addQualifier(new AutowireCandidateQualifier(ChineseLegacy.class));

            registry.registerBeanDefinition(Introspector.decapitalize(aClass.getSimpleName()), beanDefinition);
        }
    }
}
